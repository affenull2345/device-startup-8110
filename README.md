# Package device-startup

This package is part of [bananian](https://gitlab.com/affenull2345/bananian).
To build it, please use the Makefile in that repository (see its README for
more information).
